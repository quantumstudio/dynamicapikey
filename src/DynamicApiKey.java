import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class DynamicApiKey {

    private String signature;
    private long timeOut;

    public DynamicApiKey(String signature, long timeOut) {
        this.signature = signature;
        this.timeOut = timeOut;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }

    public boolean valid(String key) throws NoSuchAlgorithmException {
        String timestamp = key.substring(0, 13);
        String hash = sha1(timestamp + signature);
        Date received;

        try {
            received = new Date(Long.parseLong(timestamp));
        } catch (NumberFormatException ex) {
            return false;
        }

        // Firmas coinciden y el timestamp no es más viejo del valor de timeOut.
        return key.substring(13).equals(hash) && Math.abs(new Date().getTime() - received.getTime()) < timeOut;
    }

    private String sha1(String input) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuilder sb = new StringBuilder();

        for (byte b : result) {
            sb.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
